#ifndef Consumer_H
#define Consumer_H
#include <stdio.h>
#include "conveyer_belt.h"
#include "semaphore_args.h"
class consumer_args{
  public:
    int nanoseconds;
    std::string name;
    
    int frogConsumed = 0;
    int suckersConsumed = 0;
    int totalConsumed = 0;
    conveyer_belt *belt;
    semaphore_args *semaphores;
};
#endif
