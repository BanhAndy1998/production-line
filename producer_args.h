#ifndef Producer_H
#define Producer_H
#include <stdio.h>
#include "conveyer_belt.h"
#include "semaphore_args.h"


class producer_args{
  public:
    int nanoseconds;
    bool isFrogCandy;
    std::string name;
    int produced = 0;

    conveyer_belt *belt;
    semaphore_args *semaphores;
};
#endif
