#ifndef Semaphore_H
#define Semaphore_H
#include <stdio.h>


class semaphore_args{
  public:
    sem_t empty;
    sem_t full;
    sem_t stackLock;
    sem_t exitThread;
};
#endif
