# CXX Make variable for compiler
CXX=g++
# Make variable for compiler options
#	-std=c++11  C/C++ variant to use, e.g. C++ 2011
#	-g          include information for symbolic debugger e.g. gdb 
CXXFLAGS += -std=c++11 -pthread

# Rules format:
# target : dependency1 dependency2 ... dependencyN
#     Command to make target, uses default rules if not specified

# First target is the one executed if you just type make
# make target specifies a specific target
# $^ is an example of a special variable.  It substitutes all dependencies
wordcount : main.cpp
	$(CXX) $(CXXFLAGS) -o CandyFactory $^

Producer_args.o: Producer_args.h main.cpp

Semaphore_args.o: Semaphore_args.h main.cpp

Conveyer_belt.o: Conveyer_belt.h main.cpp

Consumer_args.o: Consumer_args.h main.cpp

