#include <pthread.h>
#include <stdio.h>
#include <string>
#include <semaphore.h>
#include <queue> 
#include <iostream>
#include <chrono>
#include <thread>
#include <unistd.h>

#include "producer_args.h"
#include "consumer_args.h"
#include "semaphore_args.h"
#include "conveyer_belt.h"

using namespace std::chrono;

/*
* The producer thread will run as long as there have been less than 100 candies produced
* The thread will make sure that there is at most 3 frog candies and 
* The semaphores will lock out other producer/consumers from accesing the queue when this thread is in use
* This thread will print out what it produced and the contents of the conveyer belt
* When producing, pushing a 2 on the queue will represent a frog candy and a 1 an escargot candy
* Lastly, the program will wait for a set time after producing to represent actual production
*/
void *producer(void *arg) {
    producer_args *args = (producer_args *)arg;
    semaphore_args *sem_args = args->semaphores;
    conveyer_belt *belt = args->belt;

    while(belt->producedCandy < 100) {
        sem_wait(&sem_args->empty);
        sem_wait(&sem_args->stackLock); //This locks other threads trying to access the stack

        if(belt->consumedCandy == 100) {
            break;
        }
        
        if(args->isFrogCandy && belt->frogsOnBelt <= 2) {
          belt->sharedQueue.push(2); 
          belt->frogsOnBelt++;
        }else{
          belt->sharedQueue.push(1); 
          belt->suckersOnBelt++;
        }
        
        belt->producedCandy++;
        args->produced++;

        std::cout << "Belt: " << belt->sharedQueue.size() << " Frogs = " << belt->frogsOnBelt << " Escargots = " << 
        belt->suckersOnBelt << ". Produced: " << belt->producedCandy << "\tAdded " << args->name << std::endl;

        if(belt->producedCandy == 99) {
          sem_post(&sem_args->stackLock); //Free the stack
          sem_post(&sem_args->full); //Unlock consumer from stack
          break;
        }
        
        sem_post(&sem_args->stackLock); //Free the stack
        sem_post(&sem_args->full); //Unlock consumer from stack
        //Sleep after produced
        std::this_thread::sleep_for(std::chrono::milliseconds(args->nanoseconds)); 
    }
    while(!belt->c2) { 
      sem_wait(&sem_args->empty);
      sem_wait(&sem_args->stackLock);
      sem_post(&sem_args->stackLock); 
      sem_post(&sem_args->full); 
      std::this_thread::sleep_for(std::chrono::milliseconds(args->nanoseconds)); 
    }
    sem_post(&sem_args->stackLock); 
    sem_post(&sem_args->full); 
    pthread_exit(NULL);
};


/*
* The consumer thread will run as long as there have been less than 100 candies consumed
* The thread will consume the first thing on the queue, no matter what type of candy it is
* The semaphores will lock out other producer/consumers from accesing the queue when this thread is in use
* This thread will print out what it consumed as well as the contents of the conveyer belt
* Lastly, the thread will wait for a set time after consuming to represent real consumption
*/
void *consumer(void *arg) {
    consumer_args *args = (consumer_args *)arg;
    semaphore_args *sem_args = args->semaphores;
    conveyer_belt *belt = args->belt;

    while(belt->consumedCandy < 100) {
        sem_wait(&sem_args->full); //Wait for the stack to get filled by producer.
        sem_wait(&sem_args->stackLock); //Lock stack

        int b = belt->sharedQueue.front(); //Grab the next candy off the stack.    
        belt->sharedQueue.pop();
        belt->consumedCandy++;
        args->totalConsumed++;

        if(b==2) { //then its a frog
          belt->frogsOnBelt--;
          args->frogConsumed++;
          std::cout << "Belt: " << belt->sharedQueue.size() << " Frogs = " << belt->frogsOnBelt << " Escargots = " << 
          belt->suckersOnBelt << ". produced: " << belt->producedCandy << "\t" << args->name << " consumed Frog Bite" << std::endl;
        }else{
          belt->suckersOnBelt--;
          args->suckersConsumed++;
          std::cout << "Belt: " << belt->sharedQueue.size() << " Frogs = " << belt->frogsOnBelt << " Escargots = " << 
          belt->suckersOnBelt << ". produced: " << belt->producedCandy << "\t" << args->name << " consumed Escargot Suckers" << std::endl;
        }
        if(belt->consumedCandy == 99) {
            sem_post(&sem_args->stackLock); //Free the stack
            sem_post(&sem_args->empty); //Unlock consumer from stack
            break;
        }

        sem_post(&sem_args->stackLock); //free queue
        sem_post(&sem_args->empty); //Unlock producer from producing

        //Sleep after consumed
        std::this_thread::sleep_for(std::chrono::milliseconds(args->nanoseconds)); 
    }
    if(belt->c1&&!belt->c2) {
        belt->c2 = true; //True when second consumer finishes
    }
    if(!belt->c1) {
        belt->c1 = true; // True when first consumer finishes
    }
    while(!belt->c2) {
      sem_wait(&sem_args->full);
      sem_wait(&sem_args->stackLock);
      sem_post(&sem_args->stackLock); 
      sem_post(&sem_args->empty); 
      std::this_thread::sleep_for(std::chrono::milliseconds(args->nanoseconds)); 
    }
    sem_post(&sem_args->stackLock); 
    sem_post(&sem_args->empty); 
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{

    int lucyWait = 0;
    int ethelWait = 0;
    int frogWait = 0;
    int escargotWait = 0;

    int Option = 0;
    
    //Partition the arguments with the getopt function
    while ((Option = getopt(argc, argv, "E:L:f:e:")) != -1) {
        switch (Option) {
            case 'E': 
                ethelWait = atoi(optarg);
            break;
            case 'L':
                lucyWait = atoi(optarg);
            break;
            case 'f': 
                frogWait = atoi(optarg);
            break;
            case 'e':
                escargotWait = atoi(optarg);
            break;
            default:
                exit(1); //Not a valid argument
        }
    }

    std::queue<int> queue;

    conveyer_belt *belt = new conveyer_belt();
    belt->sharedQueue = queue;

    sem_t empty;
    sem_t full;
    sem_t stackLock;
    
    //Initialize semaphores for use in the classes
    sem_init(&empty, 0, 10); 
    sem_init(&stackLock, 0, 1);
    sem_init(&full, 0, 0); 

    semaphore_args *semaphores = new semaphore_args();

    semaphores->empty = empty;
    semaphores->full = full;
    semaphores->stackLock = stackLock;

    //Object for ethel as a consumer
    consumer_args *ethel = new consumer_args();
    ethel->name = "Ethel";
    ethel->nanoseconds = ethelWait;
    ethel->semaphores = semaphores;
    ethel->belt = belt;
    
    //Object for lucy as a consumer
    consumer_args *lucy = new consumer_args();
    lucy->name = "Lucy";
    lucy->nanoseconds = lucyWait; 
    lucy->semaphores = semaphores;
    lucy->belt = belt;
    
    //Object for the frog bites producer
    producer_args *bites = new producer_args();
    bites->name = "Frog Bite";
    bites->nanoseconds = frogWait;
    bites->isFrogCandy = true;
    bites->semaphores = semaphores;
    bites->belt = belt;

    //Object for the escargot sucker producer
    producer_args *suckers= new producer_args();
    suckers->name = "Escargot Suckers";
    suckers->nanoseconds = escargotWait;
    suckers->isFrogCandy = false;  
    suckers->semaphores = semaphores;
    suckers->belt = belt;

    //Create all the producer and consumer threads
    pthread_t p1Thread, c1Thread, p2Thread, c2Thread;
    pthread_create(&p1Thread, 0, producer, bites) ; //Frog Bite candy thread
    pthread_create(&p2Thread, 0, producer, suckers); //EscargotSucker candy thread
    pthread_create(&c1Thread, 0, consumer, lucy); //Lucy's Consumer thread
    pthread_create(&c2Thread, 0, consumer, ethel); //Ethel's consumer thread

    pthread_join(p1Thread, NULL);
    pthread_join(c1Thread, NULL);
    pthread_join(p2Thread, NULL);
    pthread_join(c2Thread, NULL);

    std::cout << "PRODUCTION REPORT" << std::endl;
    std::cout << "--------------------" << std::endl;
    std::cout << "Crunchy frog bite producer generated " << bites->produced << " candies" << std::endl;
    std::cout << "Escargot sucker producer generated " << suckers->produced << " candies" << std::endl;
    std::cout << "Ethel consumed " << ethel->frogConsumed <<  " Crunchy frog bites + " << ethel->suckersConsumed << " Escargot suckers = " << ethel->totalConsumed << std::endl;
    std::cout << "Lucy consumed " << lucy->frogConsumed <<  " Crunchy frog bites + " << lucy->suckersConsumed << " Escargot suckers = " << lucy->totalConsumed << std::endl;

    return 0;
}
