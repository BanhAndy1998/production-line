Created by Andy Banh for CS570 at SDSU

This project is created to demonstrate how mutexes and semaphores are used in multithreading applications through the use of the producer-consumer problem.
This program in particular uses two candy consumer and two candy producer posix threads to showcase how the producer-consumer problem can be avoided.

The rules that the program runs under are as follows:
1. There are two producers, one that produces frog candies and the other escargot candies
2. There are two cosnumers, one that eats exclusively frog candies and the other escargot candies
3. If there are three frog candies on the conveyer belt then no more frog candies shall be produced until the number of frog candies drops below three
4. The producers will continue to produce candies until 100 have been made
5. The consumers will consume candies until 100 have been eaten
6. The consumers and producers must wait a certain amount of time before they are ready for their next action.

This program accepts 4 optargs for the wait time for each of the producers and consumers in nanoseconds. These optional arguments can be passed in by including -E -L -f -e along with an integer value for each option when running the program.